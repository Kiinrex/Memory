package com.exemple.profedam.memory.controllers;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.os.CountDownTimer;

import com.exemple.profedam.memory.model.Carta;

import java.util.ArrayList;


public class GeneralListener implements AdapterView.OnItemClickListener{

    MyCountDownTimer countDownTimer;
    boolean count;
    private MainActivity tauler;
    ArrayList<Carta> Cartas = new ArrayList<Carta>();
    Carta repe;
    int conta=0;

    public GeneralListener(MainActivity tauler) {
        this.tauler = tauler;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
             Toast.makeText (tauler, "position" + position, Toast.LENGTH_SHORT).show();
            // view.setVisibility(View.INVISIBLE);
        if(!count) {
            if (Cartas.size() < 2) {

                Carta cartaSeleccionada = tauler.getPartida().getLlistaCartes().get(position);
                if (cartaSeleccionada.getEstat() != Carta.Estat.FIXED) {
                    if (cartaSeleccionada != repe) {
                        cartaSeleccionada.girar();
                        tauler.refrescarTablero();

                        Cartas.add(cartaSeleccionada);
                        repe = cartaSeleccionada;
                    }
                }
            }

            if (Cartas.size() == 2) {
                repe = null;

                if (Cartas.get(0).getFrontImage() == Cartas.get(1).getFrontImage()) {
                    Cartas.get(0).fijar();
                    Cartas.get(1).fijar();

                    Cartas.clear();
                } else {
                    count = true;
                    MyCountDownTimer countDownTimer = new MyCountDownTimer(2000, 1000);
                    countDownTimer.start();
                }
            }
            boolean val = true;

            for (int h = 0; h < tauler.getPartida().getLlistaCartes().size(); h++) {
                if (tauler.getPartida().getLlistaCartes().get(h).getEstat() != Carta.Estat.FIXED) {
                    val = false;
                }
            }

            if (val == true) {
                tauler.openResultado(true);
            }
        }
    }

    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            Cartas.get(0).girar();
            Cartas.get(1).girar();
            tauler.refrescarTablero();
            Cartas.clear();
            count = false;
        }
        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}

