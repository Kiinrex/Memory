package com.exemple.profedam.memory.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.exemple.profedam.memory.R;

public class finalpartida extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalpartida);
    }

    public void empezar(View view){
        Intent i = new Intent(this, inicio.class);
        startActivity(i);
    }

}
