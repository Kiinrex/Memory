package com.exemple.profedam.memory.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.exemple.profedam.memory.R;

public class inicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }

    public void facil(View view){
        int cartas = 8;

        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("num",cartas);

        startActivity(i);
    }

    public void normal(View view){
        int cartas = 12;

        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("num",cartas);

        startActivity(i);
    }

    public void dificil(View view){
        int cartas = 16;

        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("num",cartas);

        startActivity(i);
    }

}
